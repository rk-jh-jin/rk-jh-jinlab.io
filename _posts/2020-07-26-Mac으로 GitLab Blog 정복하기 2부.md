---
layout:  post
title:  "Mac으로 GitLab Blog 정복하기 2부"
---
지난 시간에 이어서!
이번 시간에는 GitLab에서 블로그 만드는거를 할 겁니다!
~~*Git은 뭐고 GitLab은 뭐하는 곳이냐고요? Git은 소스코드를 저장할 수 있는? 그런거고 GitLab은 그걸 지원하는 곳?이랍니다! 자세한건 나중에 추가할 껍니다!*~~

1. GitLab 아이디 만들기
	일단 GitLab.com에 가서 아이디를 만듭시다! 무료예요! ~~*좋댜!*~~
	가입하면서 기입한 본인의 id를 ```userid```라고 칭할께요!
	나중에 다 쓰이니까 기억해둡시다!

2. Page 만들기
	이제 본격적으로 blog를 만들어봅시다! 근데 왜 제목이 Page 만들기냐고요?
	GitLab에서 제공하는 Page 서비스를 통해 blog를 만드는 거거든요!
	일단 GitLab에서 제공하는 어떤 서비스든간에 그것들을 통틀어서 Project라고 부른답니다. 그럼 우리는 새로운 Project를 만들어야겠지요?
	New project 클릭!
	(GitLab 메인 홈페이지 img)
	하면 4가지 선택 창?이 뜰 껍니다! 그 중 Create blank project 클릭!
	(GitLab project 선택 img)
	~~*다른 것들은 뭐냐고요? 나중에 기회되면 자세히 설명하죠! 지금은 필요없어요!*~~
	project를 page로 활용하려면 project name이  ```userid.gitlab.io```가 되어야한답니다.
	자 그렇게 적어줍시다!
	이렇게 되면 project의 주소는 ```gitlab.com/userid/userid.gitlab.io```가 된답니다. 이건 프로젝트 주소지 블로그 주소가 아닙니다! 착각 노노!
자, 그리고 밑에 보면 project description 란이 있을 겁니다. 말 그대로 project 소개란 입니다. 대충 적어줍시다. 안적어도 무방합니다!


	맨 밑에는 Private, Public, Initialize repository with a README 이렇게 3개의 체크 박스가 있을겁니다. 일단 아직 블로그를 공개 안할 것이기 때문에 Private과 어쩌구 README를 체크해주고 Create project를 눌러줍시다.
README는 말그대로 README입니다. 크게 신경 안쓰셔도 되지만 나중에 한번 읽어보세용. Private, Public은 나중에 설정에서 변경할 수 있습니다!

	자, 이렇게 project가 만들어졌습니다! 이제 이걸 자신의 Mac에 clone(복사)시켜야합니다! 
	자 terminal에서 open합시다!
	그리고 terminal에서 자신의 clone을 관리할 디렉토리로 이동합시다.
	(~~간단한 terminal 사용법 링크~~)
	저는 Documents(문서) 폴더에 Myblog 폴더를 만들어 그 폴더에서 blog를 관리하도록 세팅해보겠습니다. ~~*본인 편한 디렉토리에서 작업하시면 됩니다*~~

	자 그 디렉토리로 이동했으면 아래 코드를 입력해주세용!
	```
	$ git clone git@gitlab.com:userid/userid.gitlab.io.git  
	```
	그럼 terminal이 cloning 쏼라쏼라하면서 clone이 만들어질껍니다. finder를 통해 해당 디렉토리로 가보면 ```userid.gitlab.io```라는 폴더가 생성되어있을겁니다. 당장은 아무것도 없지만 이게 여러분의 project의 clone입니다.
	그나저나 왜 clone하냐구요? git은 말 그대로 저장소입니다. 물론 gitlab에서 파일들을 수정할 수야 있지만 그래서는 너무 불편하고 지원되는 것도 별로 없잖아요? 그래서 자신의 Mac에서 작업을 하고 그걸 git에 (저장소에) 올리는 겁니다. 그리고 이렇게 git에 있는게 본인의 블로그에 나타나게 되는 거죠. 참고로 이 때 본인의 Mac이 본인의 서버가 된답니다

3. GitLab-CI 세팅하기
	이제 .gitlab-ci.yml이란 파일을 만들어 GitLab-CI를 세팅해야합니다. 일단 GitLab 홈페이지에서 본인이 만든 project로 넘어가봅시다. 좌측에 보시면 여러가지 메뉴들이 보이는데 그 중 CI/CD를 눌러봅시다! 그러면 세부 목록으로 Pipelines, Jobs, Schedules가 있을 것이고 아래 같은 화면이 뜰겁니다.
	
	그래서 이게 대체 뭐냐고요?
CI = Continuous Integration (지속적인 통합)
CD = Continuous Delivery or Continuous Deployment (지속적인 제공 or 지속적인 배포)
의 약자입니다.
위에서 언급했듯이 일단 git은 단순 저장소라고 생각합시다. 그러나 우리의 목표는 블로그지 저장소가 아니잖아요? 그럼 저장소까지는 만들었으니 저장소에 있는 것들을 블로그 주소로 연결하여 블로그에 표시/표현해야하지 않겠어요?
즉, 본인의 Mac과 저장소를 연결했듯이 저장소와 블로그를 연결해야겠지요?
그 연결을 CI/CD라고 생각하시면 됩니다. ~~*더 자세한 내용은 추후에 다루지요!*~~

	(Mac <----------> GitLab Project ----------> Blog)
	(이런 식의 그림)

	이 .gitlab-ci.yml이란 파일을 통해 CI/CD를 자신이 원하는대로 세팅할 수 있는데 이는 나아아중에 자세히 공부하도록 하고 우리는 일단 연결만 되도록 세팅하도록 하죠!

	terminal을 열고 clone을 만들었던 디렉토리로 이동해줍시다!
	거기서 아래 코드를 입력합시다!
	```
	$ vim .gitlab-ci.yml
	```
	자 그럼 new file이라 적혀있는 창이 뜰 껍니다. 
	a키를 눌러 편집모드로 들어가서 아래 코드를 복사해서 붙여넣어주세요!
	```
	image: ruby:2.7

	pages: 
		script: 
		- gem install jekyll 
		- jekyll build -d public/ 
		artifacts: 
			paths: 
			- public 
		only: 
		- master 
	```
	ESC키를 눌러 편집모드에서 빠져나오고 ```:w```로 저장하고 ```:q```로 빠져나옵시다!

	이렇게 .gitlab-ci.yml을 만들었지만 이 파일은 아직 본인의 Mac에만 존재합니다. 이걸 GitLab에게도 보내줘야겠지요?

	terminal에서
	$ git add .gitlab-ci.yml
	$ git commit -m "Add .gitlab-ci.yml"
	$ git push origin master

	이렇게 하면 GitLab에도 파일이 추가됩니다!
	```git add, git commit -m "~~~~~", git push origin master```
	요 3가지 명령은 추후에도 자주 쓰이니 기억해둡시다!
	참고로 ```git commit -m "~~~~"```은 "~~~~"라는 comment를 적는다는 건데 이걸 안적으면 push가 안되니 꼭 적어줍시다.

4. Jekyll Blog Template 만들기
	이제 jekyll을 이용해 Blog Template를 만들겁니다. 여태까지는 이것을 위한 준비?단계라고 생각하면 지금부터가 본격적으로 Blog를 만드는게 되겠네요.
	자 terminal에서 clone이 있던 디렉토리로 이동합시다. 그리고 아래 코드를 입력해주세요.
	```
	$ jekyll new .
	```

	그러면 디렉토리에 이것저것 막 생길겁니다. 이게 기초적인 jekyll template가 되겠네요!
	그리고 terminal에서 
	```
	$ jekyll serve
	```
	을 입력하면 http://127.0.0.1:4000라는 서버가 열릴껍니다. 인터넷에서 이걸 타고 들어가면 짜잔! 아래와 같은 화면이 뜰껍니다! 

	(jekyll template 기본 화면)

5. userid.gitlab.io에 jekyll template 띄우기
	위에서 jekyll template를 만들었으니 이제 이걸 본인의 Blog에 띄워야겠죠?! 근데 본인의 Blog 주소가 뭐냐고요? userid.gitlab.io가 Blog 주소가 된답니다! 다시 terminal을 open해서 clone이 있던 디렉토리로 이동합시다! 그리고 아래 코드들을 차례로 입력해줍시다!
	```
	$ git add .gitignore
	$ git add *
	$ git commit -m "Add jekyll template"
	$ git push origin master
	```

	이렇게 하면 본인의 git에 jekyll template 관련 파일들이 추가된 것을 확인할 수 있습니다. 그런데 .gitignore가 뭐냐고요? .gitignore는 git에서 관리하지 않게 하는 폴더라고 보시면 되는데요 이 안에 있는 파일들은 git의 관리에서 벗어나게 됩니다. 자동으로 생성되는 로컬 파일 등을 git에서 관리하지 않게 하기 위해 사용한다고 하네요!

	여기까지 하시면 GitLab의 본인의 project로 들어가주세요! 그리고 CI/CD를 눌러보시면 running이란게 떠있을테고 좀 기다리시면 (약 2분? 정도) 이게 passed로 바뀔 겁니다. 그럼 이제 주소창에 ```userid.gitlab.io```를 입력해서 진입해보면 아까 jekyll serve에서 봤던 화면이 떠있을껍니다! ~~*짜잔!*~~



여기까지가 이제 GitLab에 본인의 Blog를 만드는 방법이였습니다.
이제는 Blog를 꾸며야겠죠? 다음부터는 Blog를 꾸미는 법에 대해서 알아봅시다!
To be continued!


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc1NDUzNTY2OSwtNTA5NDE4MTE0LDEzMT
c2MjUyMTEsMTkwNTYwMTI1OSw2NTk1MjM5MTIsLTM3OTU4OTE4
OF19
-->