---
layout:  post
title:  "BABA IS YOU Copy Cat"
categories: [Project]
draft: "yes"
---

# BABA IS YOU의 모바일 Copy Cat

- Swift를 이용
- SpriteKit 이용
- 이건 필자의 공부 및 재미 삼아 내 식으로 기능을 구현해본거다. 완전 쌩 초짜가 맨땅에 해딩하면서 만들었기 때문에 코드가 더럽고 알고리즘은 더 더럽다는 것을 미리 밝힌다.

## 0. BABA IS YOU 분석

- 제한적이지만 플레이어가 게임 규칙에 간섭해서 문제를 풀어나가는 독창적인 아이디어를 가진 퍼즐 게임.
	- 백문이불여일견이라고 직접 한 번 해보는 걸 추천한다. 특히 퍼즐 게임이나 머리 쓰는 게임을 좋아한다면 강추한다. ~~*꽤나 어렵다...*~~
	- 본 글에서는 Tutorial의 맵 구현, 일부 기능을 구현하는 걸 목표로 한다. ~~*(전체 구현은 너무 오래걸릴꺼 같아서 포기...)*~~
		- 구현 목표
			- Playable Character인 "BABA"를 움직여서 "win" 목표에 닿으면 "YOU WIN" 출력
			- "push", "stop", "win" 기능 구현 ("you" 기능 미구현)
	

## 1. 프로젝트 생성 및 기본 세팅
1. Xcode에서 새 프로젝트 생성하기
	<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/newproject.png" width="100%" height="100%">
	<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/newproject2.png" width="100%" height="100%">
- Product Name, Language는 당연히 Swift,  Game Technology는 SpriteKit으로 설정하고 생성
---
2. 기본 세팅하기
	<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/gamescenesks.png" width="100%" height="100%">
- GameScene.sks에서 Scene 밑에 helloLabel 삭제 (sks = SpriteKit Scene의 약자)
- 우측 표시된 곳의 Size는 iPhone SE, Landscape, Scale은 1, Anchor Point는 X: 0, Y: 0로 설정
- Size는 말 그대로 화면의 사이즈다. 그 밑에 Scale이 있는데 이는 화면의 사이즈의 크기를 배수로 조정할 수 있다.
- Landscape, Portrait는 말 그대로 가로, 세로의 화면방향을 조정한다.
- Anchor Point는 원점( *좌표평면의 0,0을 생각하면 된다* )의 위치를 결정한다. 초기값인 X: 0.5, Y: 0.5는 화면의 정중앙이 원점이 되고 위처럼 X: 0, Y: 0를 하게 되면 좌측 하단이 원점이 된다.
- SpriteKit은 자체적으로 물리엔진을 지원하기에 Gravity 항목, Physics 관련 코드들이 포함되어 있다. 그러나 우리는 물리엔진을 쓸 일이 없다!

<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/actionsks.png" width="100%" height="100%"> 

- Action.sks에서 밑에 표시된 것들을 삭제
- node들의 action을 관리하는 곳인데 우리는 쓰지않을 것이다.

<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/gameviewcontroller.png" width="100%" height="100%"> 

- GameViewController.swift에서 표시된 부분을 삭제

<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/gamesceneswift.png" width="100%" height="100%"> 

- GameScene.swift에서 표시된 didMove 위의 변수와 밑에 있는 것들을 전부 삭제
- 여기에서 전부 코딩


- 기본 세팅과 밑에 나올 코딩을 보면 SpriteKit의 기능들을 거의 쓰지 않는다. 쓰는 건 node관련된 것 뿐... 그 말은 node를 코드로 구현할 수 있으면 구태여 SpriteKit을 안쓰고 구현할 수 있다는 것이다. 필자가 SpriteKit의 기능을 거의 안 쓰는 이유는 첫번째로 이 copy cat의 목적이 필자가 swift를 모르기에 친해지기 위해서이고 두번째는 SpriteKit이 지원하는 기능들이 해당 게임에 크게 도움이 되지 않기 때문이다. SpriteKit을 훑어보니 대체로 물리엔진이 필요한 게임에 맞춰져있다. 마지막으로는 안 쓰는게 아니라 잘 몰라서 못 쓰는거다.~~왜 뭐~~

## 2. Grid 구성

- 원래 본 게임의 grid는 가로 33칸, 세로 18칸이지만 이걸 모바일에 그대로 적용하면 하나의 칸이 너무 작아진다. 어자피 Tutorial에서 필요한 칸 수는 그리 크지 않다. 그래서 가로 17칸, 세로 9칸으로 구현했다. ~~*길이 반토막*~~
	
```swift
// class에서 변수 선언 및 초기화
// Landscape일 때 세로 길이에 딱 맞춘 사이즈로 게임 화면을 만들 것이기 때문에 CGFloat을 사용
	// 아래는 필자가 사용한 변수 모음
private var columnCount: CGFloat = 17
private var rowCount: CGFloat = 9
private var rectSize: CGFloat = 0 // 모든 object node의 크기가 동일하므로 이게 node의 크기가 된다.
private var initXPosition: CGFloat = 0
private var characterSize: CGFloat = 0 // object node의 원점(중앙)의 위치를 계산하기 위해 필요한 변수. node의 길이의 절반
private var initCharacter: SKSpriteNode!
private var background: SKShapeNode!
private var mainField: [[[SKNode]]] = [] // 2차원 배열을 만들어 안에 [node]를 관리하는 변수. object node의 위치를 Index로 관리 및 파악하기 위해 사용
private var playerStorage: [Int] = [] // player의 위치(Index)를 저장하는 변수
private var nounStorage: [(SKNode, Int, Int)] = [] // 명사 object node의 위치(Index)를 저장하는 변수
private var isAndStorage: [(SKNode, Int, Int)] = [] // 조사 object node의 위치(Index)를 저장하는 변수 (근데 IS만 구현했다...)
private var verbStorage: [(SKNode, Int, Int)] = [] // 동사 object node의 위치(Index)를 저장하는 변수
private var preTextTable: [[String]] = [] // 문장의 완성을 판단하기 위한 2차원 배열
```

```swift
// 먼저 'background'라는 'node'를 만들어준다.
// 앞으로 만들어질 node는 background의 childNode가 된다.
func  makeBackground() {
	let width = frame.size.width
	let height = frame.size.height
	rectSize = height / rowCount
	let frameWidth = rectSize * columnCount
	initXPosition = (width - frameWidth) / 2
	let backgroundSize = CGRect(x: initXPosition, y: 0, width: frameWidth, height: height)
	background = SKShapeNode(rect: backgroundSize)
	addChild(background)
}
```

```swift
// grid 함수를 만들었다.
// grid 선만 그리면 되는데 각 칸을 node로 만들어서 background의 childNode로 만들어버렸다. 덕분에 node 갯수가 급증했다...
func makeGrid() {
	for y in 0..<Int(rowCount) {
		for x in 0..<Int(columnCount) {
			let gridX = initXPosition + (CGFloat(x) * rectSize)
			let gridY = CGFloat(y) * rectSize
			let rect = CGRect(x: gridX, y: gridY, width: rectSize, height: rectSize)
			let rectNode = SKShapeNode(rect: rect)
			rectNode.fillColor = .gray
			background.addChild(rectNode)
		}
	}
}
```

## 3. Tutorial.json 구성
- json 파일은 간단히 말하면 하나의 거대한 dictionary라고 보면 된다.
- object node들의 초기 위치 data를 담고 있다.
- 본 게임에 비해 grid를 줄였기에 각 object의 위치와 갯수를 조금 줄였다. 그래야 테스트하기 편하다.
	
```json
// 대충 이런 식으로 구현했다. (이건 일부분)
"picture": [
{
"type": "rock",
"coordinates": [[8, 5], [8, 4], [8, 3]]
},
{
"type": "wall",
"coordinates": [
[6, 6], [7, 6], [8, 6], [9, 6], [10, 6],
[6, 2], [7, 2], [8, 2], [9, 2], [10, 2]
]
},
{
"type": "flag",
"coordinates": [[10, 4]]
```

- decode를 위한 JsonDecode.swift 파일을 따로 만들었다.

```swift
import Foundation
import UIKit
	
struct  JsonDecode: Decodable {
let player: [CGFloat]
let  word: [JsonWordDecode]
let  picture: [JsonWordDecode]
let  noun: [JsonWordDecode]
let  verb: [JsonWordDecode]
}
	
struct  JsonWordDecode: Decodable {
let type: String
let coordinates: [[CGFloat]]
}
``` 

## 4. 초기 Object 배치
- SKSpriteNode에는 userdata라는 기능이 있다. userdata를 dictionary형태로 입력해주면 각 node들의 속성을 관리하기 한결 편해진다.

```swift
// object node의 배치는 거의 동일
func setInitPlayer() {
	if let path = Bundle.main.path(forResource: "Tutorial", ofType: "json") {
		do {
			// Tutorial.json에 있는 data 값 가져오기
			let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
			let decoded = try JSONDecoder().decode(JsonDecode.self, from: data)
				
			// object의 초기 위치(index 형태)
			let playerInitX = decoded.player[0]
			let playerInitY = decoded.player[1]
				
			// "Pic_baba"의 이미지파일을 이미지로 하는 SKSpriteNode 생성
			initCharacter = SKSpriteNode(imageNamed: "Pic_baba")
			// node의 size 지정
			initCharacter.size = CGSize(width: rectSize, height: rectSize)
			characterSize = rectSize / 2
				
			// node 실제 위치에 배치
			let playerX = initXPosition + rectSize * playerInitX + characterSize
			let playerY = rectSize * playerInitY + characterSize
			initCharacter.position = CGPoint(x: playerX, y: playerY)

			// userdata 입력
			initCharacter.userData = ["playable" : 1, "pushable" : 0, "melt" : 0, "open" : 0, "float" : 0, "win" : 0]
	
			// object node의 index를 관리하는 mainField에 object node 정보 업데이트
			mainField[Int(playerInitY)][Int(playerInitX)].append(initCharacter)

			// object node의 위치를 저장하는 저장소에 정보 업데이트
			playerStorage = [Int(playerInitX), Int(playerInitY)]

			// background에 childNode로 연결
			background.addChild(initCharacter)
		} catch {
			print(error)
		}
	}
}
```
- 나머지 object nodee도 약간 씩 손봐서 위와 같이 코딩해주면 된다.


## 5. Player 움직이기
- 이 부분은 코드가 길고 필자의 코드가 복잡하기 때문에 필자의 아이디어만 설명한다.
- playerStorage에 있는 data로 player의 index data를 받아서 mainField에서 player의 움직일 방향의 data를 판단한다.
- 밀 수 없는 없는 object가 있으면 안 움직인다.
- 밀 수 있거나 겹치는(통과)하는 object만 있고 움직일 수 있는 빈 공간이 있으면
- player로 부터 제일 멀리 있는 object부터 차례대로 움직인다.
- 즉, PUSH와 STOP의 기능이 포함되어있다.
- 이를 그림으로 간단히 나타내면 아래와 같다.
	<img src="https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/movealgorithms.png" width="100%" height="100%"> 
	

## 6. Object 상호작용 구현
- STOP,  PUSH는 위에서 구현했으므로 WIN만 남았다.
 - 너무나 간단하다. 움직일 때마다 player node와 win 속성을 가지고 있는 node의 위치가 같은지만 확인해주면 된다.

## 7. 규칙 구현(문장 완성 확인하는 함수 구현)
- 움직일 때마다 nounStorage에 있는 node들의 index를 기준으로 (X_index + 1, Y_index) 또는 (X_index, Y_index + 1)이 isAndStorage에 있는가? 있다면 (X_index + 2, Y_index) 또는 (X_index, Y_index + 2)가 verbStorage에 있는가?
- 있다면 preTextTable과 비교하여 새로만들어진 문장은 무엇이고 사라진 문장은 무엇인지 판단
- 새로 생기거나 사라진 문장의 verb와 noun을 가지고 해당 noun과 매치되는 object를 찾아서 관련 verb 속성을 변경
- 바뀐 TextTable을 preTextTable에 업데이트
```swift
// 필자가 구현한 코드의 일부분
func  textCheck() {
	var currentTextTable: [[String]] = []
	for nodeTuple **in** nounStorage {
		let node = nodeTuple.0
		let idxX = nodeTuple.1
		let idxY = nodeTuple.2
		let tempIsAndStorage = isAndStorage.map { [$0.1, $0.2] }
		let tempVerbStorage = verbStorage.map { [$0.1, $0.2] }
		// 가로줄 check
		if tempIsAndStorage.contains([idxX + 1, idxY]) {
			if tempVerbStorage.contains([idxX + 2, idxY]) {
				var tempArray: [String] = []
					for i in 0...2 {
						if i == 0 {
							let tempStr = String(node.name!.split(separator: "_")[1])
							tempArray.append(tempStr)
						} else if i == 1 {
							let tempNode = isAndStorage.filter {
							if $0.1 == idxX + 1 && $0.2 == idxY {
								return true
							} else {
								return false
							}
							}[0].0
						let tempStr = String(tempNode.name!.split(separator: "_")[1])
						tempArray.append(tempStr)
					} else {
						let tempNode = verbStorage.filter {
						if $0.1 == idxX + 2 && $0.2 == idxY {
							return true
						} else {
							return false
					}
					}[0].0
				let tempStr = String(tempNode.name!.split(separator: "_")[1])
				tempArray.append(tempStr)
			}
		}
		currentTextTable.append(tempArray)
	}
}
```

## 8. 완성품

![enter image description here](https://rk-jh-jin.gitlab.io/assets/images/posts/BABA_CopyCat/ezgif.com-video-to-gif.gif)
	

## 9. 후기

여러가지 깨달은게 있는데 다른 앱이나 게임들은 모르겠지만 규칙이 복잡해질 수 있는 퍼즐 게임들은 알고리즘의 초안을 어느 정도 미리 짜놓고 코딩하는게 좋을 거 같다. 이 글에서는 그냥 바로바로 코딩한거 같지만 저런 코드도 몇 번을 갈아 엎으면서 만들었다... 예를 들어 초기 mainField는 한 칸에 node 하나만 저장했었다. 그러나 그렇게하면 겹치는 상황에 버그가 나기때문에 node array로 싹 갈아엎었었다. 혼자 간단히 만들어보는 거였기에 금방 갈아엎었지만 만약 다른 사람들과 협업하는 거였으면... 생각만 해도 끔찍하다... 그 외에도 맨 처음에는 node에 userdata라는 기능이나 childNode탐색하는 방법 등을 몰라서 한참을 헤맸었다. 그 외에도 나름 좋은 공부가 된 것 같다.
	이걸 하게된 계기는 Python으로 알고리즘 문제만 풀다가 Swift를 접하고 그냥 공부하기에는 Swift가 익숙해지지 않아서 만들어봤다. 이 정도 만드는 것만으로도 오래걸리긴했지만 덕분에 Swift에 쪼금 익숙해졌다. 나는 쌩초짜이기에 코드들이 더럽고 복잡하고 다른 사람들은 더 깔끔하게 구현할 수 있을 것이다. 원래는 월드3?까지는 구현해보려고 했지만 월드마다 규칙을 살짝 손봐야하는 부분도 있고 간단히 시작한 프로젝트가 너무 커지는 거 같기도 하고 SpriteKit이 잘 사용되는 거는 아니라고 판단해서 여기까지만 구현을 했다. 그리고 만드는데 꽤 재밌기도 했고 깨닫는게 많은 좋은 경험이었다고 생각한다.


<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoibWFya2Rvd246IHtcbiAgICBhYmJyOi
B0cnVlLFxuICAgIGJyZWFrczogdHJ1ZSxcbiAgICBkZWZsaXN0
OiB0cnVlLFxuICAgIGRlbDogdHJ1ZSxcbiAgICBmZW5jZTogdH
J1ZSxcbiAgICBmb290bm90ZTogdHJ1ZSxcbiAgICBpbWdzaXpl
OiB0cnVlLFxuICAgIGxpbmtpZnk6IHRydWUsXG4gICAgbWFyaz
ogdHJ1ZSxcbiAgICBzdWI6IHRydWUsXG4gICAgc3VwOiB0cnVl
LFxuICAgIHRhYmxlOiB0cnVlLFxuICAgIHRhc2tsaXN0OiB0cn
VlLFxuICAgIHR5cG9ncmFwaGVyOiB0cnVlLFxuICB9XG5lbW9q
aToge1xuICAgIGVuYWJsZWQ6IHRydWUsXG4gICAgc2hvcnRjdX
RzOiB0cnVlLFxuICB9XG5hYmM6IHtcbiAgICBlbmFibGVkOiB0
cnVlLFxuICB9XG5rYXRleDoge1xuICAgIGVuYWJsZWQ6IHRydW
UsXG4gIH1cbm1lcm1haWQ6IHtcbiAgICBlbmFibGVkOiB0cnVl
LFxufVxuIiwiaGlzdG9yeSI6WzE5MzQ4Nzk5ODYsMjY3MzU1Nz
A5LC0yMDg2ODc4MzYxLC0xNTUyNDE2MTYyLC0xMTU4Nzg1NTE5
LC00NDgzMjc5MDksMzIzMzQ5NDcxLC05ODAxOTQ0NzYsLTM5MD
g1MzUwMSwxNjAzMDEwNzksMzYwNzUyNjU3LC0xNjAwNzQ5ODUz
LDIyMTcxNzU0MSwtMjg4MDEwNDU1LDEwMzUyMjgwNDQsNzUzND
MwNDcwLDEzMjY0MDQxODksLTEwMjYwMjc5NjYsLTgwMTkzNzM0
MSwxNTE0MjIxNTM4XX0=
-->