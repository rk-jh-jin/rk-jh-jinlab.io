---
layout:  post
title:  "Mac으로 GitLab Blog 정복하기 1부"
---

포트폴리오를 위해 "나도 Blog를 만들어 관리해보자!" 라고 생각하여 시작!!

아무것도 모르고 아는게 없는 나로서는 준비부터가 매우 어려웠다.
그래서 본 글은 본인 정리 겸 본인처럼 아무것도 모르는 사람이라도 따라할 수 있도록 작성하였다. (설명은 추후에 추가)

이것저것 알아보면서 GitLab Blog를 만드는게 단순히 만드는 것이 아니라 이러저러 여러가지 방법들(Hexo, Jekyll 등)이 있었는데 본인은 Jekyll을 이용한 방법을 택했다. 그게 뭔지는 잘 아직 잘 모르겠다. (나중에 추가)
~~*왜? 방법을 써논 글들을 읽어봤는데 Jekyll을 이용한 방법을 써논 글이 가장 따라하기 쉬웠으니까*~~ 

이제부터 설치해보자.



1. terminal을 열고 Homebrew를 update!

	   $ brew update
	 
2. Install ruby
	일단 rbenv를 설치해야한다.
	(~~*brew는 뭐고 rbenv는 뭐죠?! 나도 몰라요!*~~)



	```
	$ brew install rbenv
	```
	```
	$ brew install rbenv  ruby-build
	```
	```
	$ rbenv install 2.7.1 
	```
	
	설치할 수 있는 ruby의 버전은 다음의 코드로 확인할 수 있다.
	```
	$ rbenv install -l
	```

3.  그 다음으로는 ruby에서 사용되는 bundler라는 ruby의 패키지인 gem을 관리해주는 도구를 설치해야한다.
	```
	$ gem install bundler
	```
	근데 아마 여기서
	```
	ERROR:  While executing gem ... (Gem::FilePermissionError)
	```
     이런식의 Error가 날 것이다.
     이를 해결하는 방법으로는 2가지가 있다는데 하나는 sudo를 이용해서 root권한으로 설치하는 것이고 다른 방법은 rbenv를 통해 문제를 해결하는 것이다.
     전자는 추천하지 않는 방법이라고 하며 본인도 후자의 방법을 사용했다.
     (출처: https://jojoldu.tistory.com/288)
	 
	 terminal에서
	 ```
	$ rbenv versions
	```
	 라고 하면
	 ```
	* system (set by /Users/UserName/.rbenv/version)
	   2.7.1
	```
	이런식으로 나타 있을 것이다.
	 
	 이건 현재 ruby가 system ruby를 사용하고 있단 뜻이란다. 자세한거는 잘 모르니 나중에 추가하도록 하겠다.
	 일단 가장 먼저 할 일은 설치했던 ruby 2.7.1 버전을 사용하도록 바꿔주는 거다.
	```
	$ rbenv global 2.7.1
	```
	
	그리고 다시 버전을 확인하면
	```
	$ rbenv versions
	```
	```
	  system
	* 2.7.1 (set by /Users/UserName/.rbenv/version)
	```
	이렇게 2.7.1 버전으로 바뀌어 있을 것이다.
	마지막으로 rbenv PATH를 추가하기 위해 본인의 쉘 설정 파일(`..zshrc`, `.bashrc`)를 열어 다음의 코드를 추가한다. (이게 뭔소리지)
	저는 zshrc를 열었습니다.
	```
	$ vim ~/.zshrc
	```
	vim은 일종의 terminal에서의 텍스트 편집기라고 생각하면 되는데 자세한거는 저도 잘 모르고 대략적인 사용법만 아니까 이것도 나중에 추가
	아마 저처럼 쉘 설정 파일이 뭔지도 모르는 사람은 그냥 ~/.zshrc의 제목을 가진 새로운 빈 파일이 생겼을 껍니다.
	키보드의 a 버튼을 눌러 편집모드로 들어가주고 (편집모드로 들어가게 되면 맨 밑에 INSERT라고 글자가 뜹니다.)
	```
	[[ -d ~/.rbenv  ]] && \
	  export PATH=${HOME}/.rbenv/bin:${PATH} && \
	  eval "$(rbenv init -)"
	```

	위의 코드를 복사해서 붙여넣어주고 ESC를 누르면 편집모드에서 빠져나갑니다.
	그리고 ``:w``를 입력하면 저장이 되고 ``:q``를 누르면 vim을 빠져나오게 됩니다.
	이렇게 간단하게라도 vim을 다루는 것은 뒤에 가서도 쓰이니까 기억해둡시다.

	위의 작업을 마치면 source로 코드를 적용시킵시다! (나도 뭔소리인지 몰라!)
	```
	$ source ~/.zshrc
	```

	그리고 다시
	```
	$ gem install bundler
	```
	하면 정상적으로 설치가 될 겁니다!
	
	여기까지 ruby 관련된 건 설치 끝!

4. Install jekyll
	
	이번에는 Jekyll을 설치할 차례 (jekyll에 대한 설명은 나중에 추가)

	terminal을 열고 아래 명령 입력!
	
	```
    $ gem install jekyll
	```
	끝! 간단!

일단 1부 끝!
다음부터는 본격적으로 GitLab을 이용해서 블로그를 만들 것이도다!
To be continued!
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ0NTYzMDY3XX0=
-->