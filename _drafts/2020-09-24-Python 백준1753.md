---

layout: post

title: "Python 백준1753"

---

[백준1753번 문제](https://www.acmicpc.net/problem/1753)

기본적인 다익스트라 알고리즘 문제이다.
가중치가 존재하는 간선이 있는 그래프가 주어질 때 각 정점으로의 최소 가중치를 구하는 문제. (참고로 이 문제는 방향성이 존재한다.)

우선순위 배열인 heapq를 이용하면 간단하다.

dist라는 배열에 각 정점에 해당하는 index에 매우 큰 수를 저장해놓고 작은 값이 나올 때마다 저장해주고 마지막에 각 값을 print하면 된다.

```python
import sys
from heapq import heappop, heappush
input = sys.stdin.readline
V, E = map(int, input().split())
K = int(input())
inf = 100000000

#make graph
graph = {i:[] for i in  range(1, V + 1)}
for i in  range(E):
	u, v, w = map(int, input().split())
	graph[u].append([w, v])

#최소값을 담을 dist배열 생성
dist = [inf] * (V + 1)
dist[K] = 0

#heapq 생성
stack = []
heappush(stack, [0, K]) 


while stack:
	w, p = heappop(stack)
	for i in graph[p]:
		newW = w + i[0]
		if newW < dist[i[1]]:
			dist[i[1]] = newW
			heappush(stack, [newW, i[1]])
			
for i in dist[1:]:
	print(i if i != inf else  "INF")
```

<img  src="https://rk-jh-jin.gitlab.io/assets/images/posts/BaekJoon/B1753_answer.png"  width="100%"  height="100%">


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3NDQ1ODcwNjgsLTkzNzM1NzQ1LC0xMT
k0NDExMjU1LDI2MzQyMDM1N119
-->